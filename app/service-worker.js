const version = 1247 + parseInt(Math.floor((Math.random()*40)+5))
const cachePrefix = 'carStats-'
const staticCacheName = `${cachePrefix}static-${version}`
const expectedCaches = [staticCacheName]

addEventListener('install', event => {
  event.waitUntil((async () => {
    const cache = await caches.open(staticCacheName)

    await cache.addAll([
      './images/speedometer.png',
      './images/navigation.svg',
      './images/statistiques.svg',
      './images/parametres.svg',
      './images/pantheon.svg',
      './images/signal-satellite.svg',

      './index.php',
      './pages/navigation.html',
      './pages/statistiques.html',
      './pages/parametres.html',
      './pages/pantheon.html',

      './styles/reset.css',
      './styles/style.css',
      './styles/menu.css',
      './styles/gps_signal.css',

      './lib/jQuery.3.3.1.min.js',
      './lib/noSleep.0.6.0.min.js',

      './algorithms/comfort/noSleep_script.js',
      './algorithms/comfort/transitions.js',

      './algorithms/accelerometer.js',
      './algorithms/calc_vitesse_max.js',
      './algorithms/calc_vitesse_moy.js',
      './algorithms/geoSpeed.js',
      './algorithms/gps_accuracy.js',
      './algorithms/init_const_var.js',
      './algorithms/starter.js',
      './algorithms/units_toggle.js'
    ])

    self.skipWaiting()
  })())
})

addEventListener('activate', event => { //active le nouveau cacheStorage et supprime les anciens cacheStorage
  event.waitUntil((async () => {
    for (const cacheName of await caches.keys()) {
      if (!cacheName.startsWith(cachePrefix)) continue
      if (!expectedCaches.includes(cacheName)) await caches.delete(cacheName)
    }
  })())
})

addEventListener('fetch', event => { //détecte des évenements du navigateur
  event.respondWith(
    fetch(event.request).catch(function () {
      return caches.match(event.request)
    })
  )
})

let deferredPrompt
addEventListener('beforeinstallprompt', event => { //propose d'installer l'application
  event.preventDefault()
  deferredPrompt = event
})
