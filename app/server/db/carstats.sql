-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 15 mars 2019 à 09:21
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carstats`
--

-- --------------------------------------------------------

--
-- Structure de la table `users_data`
--

DROP TABLE IF EXISTS `users_data`;
CREATE TABLE IF NOT EXISTS `users_data` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_user_id` int(11) DEFAULT NULL,
  `user_maxspeed` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`data_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users_data`
--

INSERT INTO `users_data` (`data_id`, `data_user_id`, `user_maxspeed`) VALUES
(1, 30, 45),
(2, 31, 28),
(3, 33, 0);

-- --------------------------------------------------------

--
-- Structure de la table `users_info`
--

DROP TABLE IF EXISTS `users_info`;
CREATE TABLE IF NOT EXISTS `users_info` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT '',
  `motdepasse` text,
  `tokenpass` varchar(64) DEFAULT '',
  `role` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users_info`
--

INSERT INTO `users_info` (`id`, `pseudo`, `mail`, `motdepasse`, `tokenpass`, `role`) VALUES
(30, 'pamplemousseFou', 'pamlemousse_horticole@gmail.ru', '$2y$10$oYGDno7m7kFfx7yP7KCsV./IpG3HCI/QMcArWdtmtx55Ld8Y.v5wq', '075a83e47ca27b7729a42e10717cc3deaa9f591876baced32e90be336fea6c5d', 0),
(31, 'Pierre', 'pierreleloup33@live.fr', '$2y$10$eiYaoq4aQQ0Q2S3neyMVzu40k.u6XMSUXy528340HqZvScRMBtC6O', '530c3d120dfc7ebea0432708230cb3f2cc4ae55ac2aa5dcb334f112cac8219b3', 0),
(33, 'Pierre@sdglm.fr', 'Pierre@sdglm.fr', '$2y$10$yxwPYqYEbYeup61OaSefGu19fLeHwWXK3kz7PRkPinnx6heKjOo8S', '246edd4235d26cc88828bd79ef3944c7e14373091a3be2b52314ce4d2573513d', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
