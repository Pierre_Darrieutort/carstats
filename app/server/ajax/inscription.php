<?php

$trustCheck = 0;
$placement = 0;

try{
    require_once('../db/db.php');
    $connect_db = true;
}
catch(PDOException $ex){
    $connect_db = false;
}
if ($connect_db === true) {
    $res = "Connecté à la BDD";
        if (
        isset($_POST['pseudoForm']) &&
        isset($_POST['emailForm']) &&
        isset($_POST['mailConfirmForm']) &&
        isset($_POST['passForm']) &&
        isset($_POST['passConfirmForm']) &&
        !empty($_POST['pseudoForm']) &&
        !empty($_POST['emailForm']) &&
        !empty($_POST['mailConfirmForm']) &&
        !empty($_POST['passForm']) &&
        !empty($_POST['passConfirmForm'])) {
            $pseudo = trim(htmlspecialchars($_POST['pseudoForm']));
            $mail = trim(htmlspecialchars($_POST['emailForm']));
            $mail2 = trim(htmlspecialchars($_POST['mailConfirmForm']));
            $mdp = trim(htmlspecialchars($_POST['passForm']));
            $mdp2 = trim(htmlspecialchars($_POST['passConfirmForm']));
            $mdpHash = password_hash($mdp, PASSWORD_DEFAULT);
            $mdpVerify = password_verify($mdp2, $mdpHash);

            if(strlen($pseudo) <= 30) {
                $reqpseudo = $bdd -> prepare("SELECT pseudo FROM users_info  WHERE pseudo = ?");
                $reqpseudo -> execute(array($pseudo));
                $pseudoExist = $reqpseudo -> rowCount();
                if($pseudoExist == 0) {
                    if($mail === $mail2) {
                        if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                            $reqmail = $bdd -> prepare("SELECT mail FROM users_info  WHERE mail = ?");
                            $reqmail -> execute(array($mail));
                            $mailExist = $reqmail -> rowCount();
                            if($mailExist == 0) {
                                if($mdpVerify == 1) {
                                    $tokenpass = bin2hex(openssl_random_pseudo_bytes(32));
                                    $insertmbr = $bdd -> prepare("INSERT INTO users_info  (pseudo, mail, tokenpass, motdepasse) VALUES (?, ?, ?, ?)");
                                    $insertmbr -> execute(array($pseudo, $mail, $tokenpass, $mdpHash));
                                    $res = "Votre compte a bien été créé, vous pouvez vous connecter !";
                                    $trustCheck = 1;
                                    $placement = 0;
                                } else {
                                    $res = "Vous mots de passe ne correspondent pas";
                                    $placement = 4;
                                }
                            } else {
                                $res = "Adresse mail déjà attribuée";
                                $placement = 3;
                            }
                        } else {
                            $res = "Votre mail n'est pas valide";
                            $placement = 3;
                        }
                    } else {
                        $res = "Vos deux mail ne correspondent pas";
                        $placement = 5;
                    }
                } else {
                    $res = "Ce pseudo est déjà utilisé";
                    $placement = 1;
                }         
            } else {
                $res = "Votre pseudo ne doit pas dépasser 30 caractères";
                $placement = 1;
            }
        } else {
            $res = "Tous les champs doivent être complétés";
        }
} else {
    $res = 'Erreur de connexion à la BDD';
}

if ($placement === 0) {
    $placement = '.global';
} else if ($placement === 1) {
    $placement = '.pseudo';
} else if ($placement === 2) {
    $placement = '.pass';
} else if ($placement === 3) {
    $placement = '.mail';
} else if ($placement === 4) {
    $placement = '.passConfirm';
} else if ($placement === 5) {
    $placement = '.mailConfirm';
}

if (isset($trustCheck) && $trustCheck === 1) {
    $array = array(
        'trustRes' => $trustCheck,
        'res' => $res,
        'placement' => $placement
    );
} else {
    $array = array(
        'trustRes' => $trustCheck,
        'res' => $res,
        'placement' => $placement
    );
}
register_shutdown_function(die(json_encode($array, JSON_UNESCAPED_UNICODE)));
?>
