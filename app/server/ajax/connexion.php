<?php

$userId = isset($_POST['hidden_userId']) ? trim(htmlspecialchars($_POST['hidden_userId'])) : null;
$userHash = isset($_POST['hidden_userHash']) ? trim(htmlspecialchars($_POST['hidden_userHash'])) : null;
$trustCheck = 0;
// $placement = 0;

try{
    require_once('../db/db.php');
    $connect_db = true;
}
catch(PDOException $ex){
    $connect_db = false;
}

if ($connect_db === true) {
    $res = "Connecté à la bdd";
    if (!isset($userId) || empty($userId) || !isset($userHash) || empty($userHash)) {
        if (
        isset($_POST['pseudoForm']) &&
        isset($_POST['passForm']) &&
        !empty($_POST['pseudoForm']) &&
        !empty($_POST['passForm'])) {
            $pseudoConnect = trim(htmlspecialchars($_POST['pseudoForm']));
            $mdpConnect = trim(htmlspecialchars($_POST['passForm']));
            $userSearch = $bdd -> prepare('SELECT id, motdepasse FROM users_info  WHERE pseudo = ?');
            $userSearch -> execute(array($pseudoConnect));
            $verifyUser = $userSearch -> fetch();

            $mdpConnectSecure = password_verify($mdpConnect, $verifyUser['motdepasse']);
            
            if(!empty($pseudoConnect) && !empty($mdpConnect)) {
                $uniqueUser = $userSearch -> rowCount();
                if($uniqueUser == 1 && $mdpConnectSecure == true) {
                    $userId = $verifyUser['id'];
                    $userHash = $verifyUser['motdepasse'];
                    $trustCheck = 1;
                    $res = "Connecté";
                } else {
                    $res = "Mauvais identifiant ou mot de passe";
                }
            } else {
                $res = "Tous les champs doivent être complétés";
            }
        }
    } else {        
        $userTrust = $bdd -> prepare('SELECT COUNT(*) FROM users_info WHERE id = ? AND motdepasse = ?');
        $userTrust -> execute(array($userId, $userHash));
        $trustedValue = $userTrust -> fetch();
        if ($trustedValue[0] == 1) {
            $trustCheck = 1;
            $res = "Déjà connecté";
        }
    }
} else {
    $res = "Erreur de connexion à la BDD";
}


// if ($placement === 0) {
//     $placement = '.global';
// } else if ($placement === 1) {
//     $placement = '.pseudo';
// } else if ($placement === 2) {
//     $placement = '.pass';
// }

if (isset($trustCheck) && $trustCheck === 1) {
    $array = array(
        'trustRes' => $trustCheck,
        'res' => $res,
        'userId' => $userId,
        'userHash' => $userHash,
        // 'placement' => $placement
    );
} else {
    $array = array(
        'trustRes' => $trustCheck,
        'res' => $res
    );
}
register_shutdown_function(die(json_encode($array, JSON_UNESCAPED_UNICODE)));
