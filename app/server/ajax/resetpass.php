<?php

require('../db/db.php');



if (isset($_SESSION['id']) && !empty($_SESSION['id'])) {
    header("Location: ../index.php");
} else if(isset($_POST['formlostpass'])) {
    $email = trim(htmlspecialchars($_POST['email']));
    if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $nRows = $bdd->prepare("SELECT count(*) FROM users_info WHERE mail = ?");
        $nRows->execute(array($email));
        $retour_nRows = $nRows->fetchColumn();
        if ($retour_nRows == 1) {
            
            $reqtokenpass = $bdd->prepare("SELECT tokenpass FROM users_info WHERE mail = ?");
            $reqtokenpass->execute(array($email));
            $tokenpassinfo = $reqtokenpass->fetch();
            $tokenpasslink = '/resetpass.php?tpi='.$tokenpassinfo[0];
            
            $reset_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
            $reset_url .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
            $reset_url .= $tokenpasslink;
            
            
            $Nom = explode("@", $email, 2);
            $Nom = $Nom[0];

            $Contenu= 'Si vous souhaitez r&eacute;initialiser votre mot de passe, cliquez sur ce lien: <b><i><a href="'.$reset_url.'">Modifier mon mot de passe ICI</a></b></i><br><br><i>Cet e-mail est automatique, inutile d\'y répondre.</i>';
            $Objet="Réinitialisation de vos identifiants - carStats";
		
		$Header='From: "'.$Nom.'"<'.$email.">\r\n".
		 'X-Mailer: PHP/'.phpversion();
		$Header.='MIME-Version: 1.0'."\r\n";
		$Header.='Content-type: text/html; charset=UTF-8'."\r\n";
		$Header.='Content-Transfer-Encoding: 8bit';

        $Header.= "X-Priority: 1 (Highest)\r\n";
        $Header.= "X-MSMail-Priority: High\r\n";
        $Header.= "Importance: High\r\n";

		
        mail($email,$Objet,$Contenu,$Header);
        echo('Votre mail a bien été envoyé !');




            

            // $to  = $email;

            // $subject = 'Réinitialisation de vos identifiants - carStats';

            // $message = 'Si vous souhaitez r&eacute;initialiser votre mot de passe, cliquez sur ce lien: <b><i><a href="'.$reset_url.'">Modifier mon mot de passe ICI</a></b></i><br><br><i>Cet e-mail est automatique, inutile d\'y répondre.</i>';

            // $headers[] = 'MIME-Version: 1.0';
            // $headers[] = 'Content-type: text/html; charset=utf-8';
            // $headers[] = 'From: support@carstats.fr';

            // mail($to, $subject, $message, implode("\r\n", $headers));
            // var_dump(mail($to, $subject, $message, implode("\r\n", $headers)));
            $success = "Vos informations de connexion ont étées récupérées et vont vous être envoyées par mail.";
            
        } else {
            $erreur = "L'adresse entrée est invalide";
        }
    } else {
        $erreur = "Vous devez entrer un mail valide";
    }
}



?>
<div class="login">
    <div class="lostpass-screen">
        <h1 style="margin-bottom: 50px;">Récupération</h1>

        <form class="formlostpass" method="POST" action="">
            <input required="required" id="email" type="email" class="inepoute centerholder" value="" placeholder="Votre adresse mail" name="email">
                <br>
                <?php
                    if(isset($erreur)) {
                    echo '<font color="red">'.$erreur."</font>";
                    }
                    if(isset($success)) {
                    echo '<font color="green">'.$success."</font>";
                    }
                ?>
                <br><br>
                <input type="submit" id="loggin-btn" class="inepoute" name="formlostpass" value="Envoyer informations" /><br><br>
                <a class="inepoute login-link" href="connexion.php">Retour</a>
    </div>
</div>
