<?php
session_start();

// $userId = $_SESSION['id'];
// $vMax = $_POST['vMax'];

$userId = 31;
$vMax = 76;

if (!isset($userId) || empty($userId)) {
    //récupérer derniers identifiants dans le cache ?
}
if (isset($userId)) {
    if (isset($vMax)) {
        try{
            require_once('../db/db.php');
            $connect_db = true;
        }
        catch(PDOException $ex){
            $connect_db = false;
        }


        if ($connect_db === true) {
            $res = "Connecté à la bdd";

            $dataExists = $bdd->query('SELECT COUNT(*) FROM users_data WHERE data_user_id = '. $userId)->fetch();

            if ($dataExists[0] == 1) {

                $db_recupSpeed = $bdd->query('SELECT user_maxspeed FROM users_data WHERE data_user_id = '. $userId)->fetch();
                $db_maxSpeed = intval($db_recupSpeed[0]);
                
                if ($vMax > $db_maxSpeed) {
                    $newMaxSpeed = $bdd->prepare("UPDATE users_data SET user_maxspeed = ? WHERE data_user_id = ?");
                    $newMaxSpeed->execute(array($vMax, $userId));
                    $db_maxSpeed = $vMax;
                    $res = 'Vitesse Max mise à jour';
                } else {
                    $res = "Vitesse Max non dépassée";
                }

            } else {

                $newMaxSpeed = $bdd->prepare("INSERT INTO users_data VALUES user_maxspeed = ? WHERE user_id = ?");
                $newMaxSpeed->execute(array($vMax, $userId));
                $db_maxSpeed = $vMax;
                $res = "Première vitesse max atteinte, ligne créée";

            }
        } else {
            $res = "Erreur de connexion à la bdd";
        }
    } else {
        $res = 'Pas de vitesse reçue';
    }
} else {
    $res = 'Pas de user id reçu';
}


$array = array(
    'res' => $res,
    'vMax' => $vMax,
    'return' => $db_recupSpeed[0]
);


echo json_encode($array, JSON_UNESCAPED_UNICODE);
?>
