$(document).ready(function () {
    pageLoadMethod()
})

const pageLoadMethod = () => {
    const pageName = window.location.href.split('/').pop()
    pageName == '' ? firstLoad() : lambdaLoad()
}

const firstLoad = () => {
    window.history.pushState(null, null, pageVueParam + page1)

    var pageName = pagesFolder + window.location.href.split(pageVueParam).pop()
    backMenuSelectorMovement(pageName.split('/').pop())

    $(wrapper).fadeOut(250, function () {
        $(wrapper).load(pageName, function () {
            $(wrapper).fadeIn(250)
        })
    })
}

const lambdaLoad = () => {
    var pageName = window.location.href.split(pageVueParam).pop()
    backMenuSelectorMovement(pageName)

    $(wrapper).fadeOut(250, function () {
        $(wrapper).load(pagesFolder + pageName, function () {
            $(wrapper).fadeIn(250)
        })
    })
}


$(window).on('popstate', function () {
    pageLoadMethod()
})

$('nav a').on('click', function (event) {
    event.preventDefault()

    const href = $(this).attr('href')

    window.history.pushState(null, null, href)

    pageLoadMethod()
})

const backMenuSelectorMovement = (page) => {
    switch (page) {
        case page1:
            $('#backMenuSelector').css('left', '0')
            break
        case page2:
            $('#backMenuSelector').css('left', '25%')
            break
        case page3:
            $('#backMenuSelector').css('left', '50%')
            break
        case page4:
            $('#backMenuSelector').css('left', '75%')
            break
    }
}


//* miscellaneous loads *//

const loadSignMethod = (url) => {
    $(wrapper).fadeOut(250, function () {
        $(wrapper).load(url, function () {
            $(wrapper).fadeIn(250)
        })
    })
}

const redirectLoadMethod = (url) => {
    $(wrapper).fadeOut(250, function () {
        $(wrapper).load(pagesFolder + url, function () {
            $(wrapper).fadeIn(250)
        })
    })
}

const loadInId = (id, path, file, speedStart, speedEnd) => {
    $(id).fadeOut(speedStart ? speedStart : 250, function () {
        $(id).load(path + file, function () {
            $(id).fadeIn(speedEnd ? speedEnd : 250)
        })
    })
}
