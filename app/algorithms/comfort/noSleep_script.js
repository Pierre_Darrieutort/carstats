noSleep_script = () => {
    if (!wakeLockEnabled) {
        noSleep.enable()
        wakeLockEnabled = true
        //$('#averageSpeed').text("Wake Lock Activé")
    } else {
        noSleep.disable()
        wakeLockEnabled = false
        //$('#averageSpeed').text("Wake Lock Désactivé")
    }
}
