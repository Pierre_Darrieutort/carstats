const initParams = () => {
    //* Night Storage *//
    var nightStorage = localStorage.getItem('nightToggle')
    const nightToggle = document.querySelector('#nightToggle')
    
    if (nightStorage == 'true') {
        nightToggle.checked = true
    } else {
        nightToggle.checked = false
    }

    //* Mile Storage *//
    var mileStorage = localStorage.getItem('mileToggle')
    const mileToggle = document.querySelector('#mileToggle')

    if (mileStorage == 'true') {
        mileToggle.checked = true
    } else {
        mileToggle.checked = false
    }

    //* Language Storage *//
    var langStorage = localStorage.getItem('lang')
    const langSelector = $('[name=optLang]')

    if (langStorage == null) {
        localStorage.setItem('lang', user.default_lang)
    }
    langSelector.val(localStorage.getItem('lang'))

    //* DriveStyle Storage *//
    var driveStorage = localStorage.getItem('driveStyle')
    const driveSelector = $('[name=optDrive]')

    if (driveStorage == null) {
        localStorage.setItem('driveStyle', user.default_driveStyle)
    }
    driveSelector.val(localStorage.getItem('driveStyle'))
}

const nightToggle = () => {
    var toggle = $('#nightToggle:checked').val()
    if (toggle == undefined || toggle == null || toggle == false) {
        localStorage.setItem('nightToggle', false)
    } else {
        localStorage.setItem('nightToggle', true)
    }
}

const mileToggle = () => {
    var toggle = $('#mileToggle:checked').val()
    if (toggle == undefined || toggle == null || toggle == false) {
        localStorage.setItem('mileToggle', false)
    } else {
        localStorage.setItem('mileToggle', true)
    }
}

const dumpStats = () => {
    var r = confirm("Supprimer mes statistiques ?");
    if (r == true) {
        $.ajax({
            type: "POST",
            url: "server/ajax/delete_stats.php",
            dataType: 'json',
            data: {
                userId: userId
            },
            success: function (cb) {
                console.log(cb)
                alert('Vos statistiques ont été supprimées')
            },
            error: function () {
                console.log('erreur')
                alert('Erreur durant la suppression.')
            }
        })
    }
}

const disconnect = () => {
    if (confirm("Se déconnecter ?")) {
        localStorage.removeItem('userId')
        localStorage.removeItem('userHash')
        $('.connected').css('display', 'none')
    } else {
        return false
    }
}

const optLang = () => {
    localStorage.setItem('lang', $('[name=optLang]').val())
    initLang()
}

const optDrive = () => {
    localStorage.setItem('driveStyle', $('[name=optDrive]').val())
}

const editPseudo = () => {

}

const editMail = () => {

}

const editPass = () => {

}
