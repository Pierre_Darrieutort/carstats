const starterButton = () => {

    const starter = document.querySelector('#start')
    const readout = document.querySelector('#readout')

    //? Lancement de fonctions à l'affichage de la page
    noSleep_script()
    gps_accuracy()

    if (appOpts.watchId) {
        //* Extinction du système, système éteint
        navigator.geolocation.clearWatch(appOpts.watchId)

        appOpts.watchId = null

        starter.textContent = '\u{1F511} Start'
        starter.classList.toggle('selected')

        readout.textContent = '-'

        duree[1] = Math.round(new Date().getTime() / 1000)

        clearInterval(interval_0)
        clearInterval(interval_1)
        clearInterval(interval_10)
        clearInterval(interval_1000)
        clearInterval(ticksTransition)

        started = 0

        dureeTrajet()
    } else {
        //* Lancement du système, système en fonctionnement
        resetStarter()

        const options = {
            enableHighAccuracy: true
        }
        appOpts.watchId = navigator.geolocation.watchPosition(parsePosition, null, options)

        starter.textContent = '\u{1F6D1} Stop'

        readout.textContent = '...'

        duree[0] = Math.round(new Date().getTime() / 1000)

        depart = moment().valueOf()
        
        $('#legal').text(limitation)

        //? Lancement au start puis lancement des intervales
        gettingMaxSpeed() // = vitesse maximum
        vitesse_to_tab() // = vitesse moyenne
        valueToggle() // = Changement des valeurs selon unité
        gps_accuracy() // = Qualité du signal GPS
        graphSpeed(0, 1) // = Affichage de la jauge en ticks
        dureeTrajet() // = Affichage la durée du trajet
        displayDebut() // = Affichage date de départ du trajet
        //! À DÉCOMMENTER !//
        // speedLimit(lat, long) // = Obtient la limitation de vitesse
        //! À DÉCOMMENTER !//

        interval_0 = setInterval(() => {
            $('#readout').text(vitesse)
        }, 250)
        interval_1 = setInterval(() => {
            gettingMaxSpeed()
            vitesse_to_tab()
            if (window.location.href.split('=').pop() == 'navigation.html') {
                graphSpeed(vitesse, limitation)
                valueToggle()
            }
            //! PHASE DE TEST ----- À RETIRER ABSOLUMENT !//
            // pre_vitesse = Math.floor((Math.random() * 40) + 5)
            pre_vitesse = 16.7
            pre_limitation = 50
            //! PHASE DE TEST ----- À RETIRER ABSOLUMENT !//
        }, 1000)
        interval_10 = setInterval(() => {
            if (window.location.href.split('=').pop() == 'navigation.html') {
                gps_accuracy()
            }
            //! À DÉCOMMENTER !//
            // speedLimit(lat, long)
            //! À DÉCOMMENTER !//
        }, 10000)
        interval_1000 = setInterval(() => {
            $('#legal').text(limitation)
            dureeTrajet()
        }, 30000)
    }
}
