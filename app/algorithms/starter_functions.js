//? Niveau de précision du GPS
const gps_accuracy = () => {
    const signal = document.querySelector('#gps_Signal')

    var accuracy = Math.round(pre_accuracy)
    signal.classList.remove('q1', 'q2', 'q3', 'q4', 'q5')

    if (accuracy <= 3) {
        signal.classList.add('q5')
    } else if (accuracy <= 6) {
        signal.classList.add('q4')
    } else if (accuracy <= 10) {
        signal.classList.add('q3')
    } else if (accuracy <= 14) {
        signal.classList.add('q2')
    } else if (accuracy > 14) {
        signal.classList.add('q1')
    }
}

//? Extraction de données de position
const parsePosition = (position) => {
    //* Se lance à chanque changement de coordonnées
    pre_vitesse = position.coords.speed
    pre_accuracy = position.coords.accuracy
    lat = position.coords.latitude
    long = position.coords.longitude
}

//? Mise en tableau de la vitesse lors des 5 dernières secondes
vitesse_to_tab = () => {
    if (pre_vitesse > 0) {
        tableau_vitesses.push(pre_vitesse)
    }
    if ($.isEmptyObject(tableau_vitesses)) {
        var somme = 0
    } else {
        var somme = tableau_vitesses.reduce(function (a, b) {
            return a + b
        })
    }
    if (tableau_vitesses.length == 5) {
        // Une requête par seconde, donc refresh de la moyenne toutes les 5 secondes
        pre_distance += somme
        tableau_vitesses = []
        vitesse_moyenne(pre_distance, total_rows_tab)
    }
    total_rows_tab++
}

//? Obtention de la vitesse moyenne
vitesse_moyenne = (a, b) => {
    pre_vMoy = parseFloat(a / b)
}

//? Obtention de la vitesse Max
gettingMaxSpeed = () => {
    if (pre_vitesse > newMaxSpeed) {
        newMaxSpeed = pre_vitesse
        pre_vMax = parseFloat(newMaxSpeed)
        ajax_vMax(pre_vMax)
    }
}
//? Envoi de la vitesse Max
ajax_vMax = (vitesse) => {
    $.ajax({
        type: "POST",
        url: "server/ajax/ajax_vMax.php",
        dataType: 'json',
        data: {
            vMax: vitesse
        },
        success: function (cb) {
            console.log(cb)
        },
        error: function () {
            console.log('erreur ajax_vMax')
        }
    })
}

//? Accéléromètre
// var tableau_1 = [1, 2, 2, 3, 4]
// var tableau_2 = [1, 2, 3, 3, 3]
// const tableau = tableau_2
// var acceleration = instanceOfDeviceMotionEvent.acceleration;
// if (tableau.length >= 3) {
//     var slicedTab = tableau.slice(Math.max(tableau.length - 3, 1))
//     if (slicedTab[0] === slicedTab[1] && slicedTab[0] === slicedTab[2]) {

//         document.querySelector('#test').textContent = acceleration
//     }
// }


//? Limitation de vitesse
const speedLimit = (lat, long) => {
    var url = "https://route.cit.api.here.com/routing/7.2/calculateroute.json?jsonAttributes=1&waypoint0=" + lat + "," + long + "&waypoint1=" + lat + "," + long + "&departure=2019-01-18T10:33:00&routeattributes=sh,lg&legattributes=li&linkattributes=nl,fc&mode=fastest;car;traffic:enabled&app_code=inVc1gDCNQCFSPEaRqFg8g&app_id=LfVLSnyDc8q6HKXY6VWQ"

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        success: function (cb) {
            pre_limitation = cb.response.route['0'].leg['0'].link['0'].speedLimit
        },
        error: function () {
            console.log('erreur speedLimit')
        }
    })
}


//? Vitesse & limitation sur compteur de vitesse
const graphSpeed = (vNow, vLim) => {
    if (typeof test !== 'undefined') {
        clearInterval(test)
        console.log('Interval cleared')
    }

    percentSpeed = (vNow * 100) / vLim
    ticksOfLimit = parseInt(Math.round((percentSpeed * gaugeTicks) / 100))

    if (ticksOfLimit > gaugeTicks) {
        ticksOfLimit = gaugeTicks
    }

    const turnTicks = (ticks, status, last) => {

        status == 0 ? $('#gaugeTicksWrapper>line:not(:nth-child(-n+' + ticks + '))').removeClass('stroked').css('stroke', 'var(--clightgrey)') : $('#gaugeTicksWrapper>line:nth-child(-n+' + ticks + ')').addClass('stroked')
        legalTicks()
        if (last == 1) {
            clearInterval(ticksTransition)
        }
    }

    const legalTicks = () => {
        if (vNow <= vLim) {
            $('.stroked').css('stroke', 'var(--cgreen)')
        } else if (vNow > vLim && vNow <= (vLim + 5)) {
            $('.stroked').css('stroke', 'var(--corange)')
        } else {
            $('.stroked').css('stroke', 'var(--cred)')
        }
        oldValue[0] = ticksOfLimit
    }

    var i = oldValue[0],
        u = 0,
        ticksTransition = setInterval(() => {
            if (i < ticksOfLimit) {
                turnTicks(i, 1, 0)
                u = 1
                i++
            } else if (i > ticksOfLimit) {
                turnTicks(i, 0, 0)
                u = 0
                i--
            } else if (i == ticksOfLimit) {
                turnTicks(i, u, 1)
            }
        }, 1000 / ticksOfLimit)
}

const dureeTrajet = () => {
    duree[1] = Math.round(new Date().getTime() / 1000)
    duree[2] = duree[1] - duree[0]
    displayDuree()
}

const displayDuree = () => {
    if (window.location.href.split('=').pop() == 'navigation.html') {
        const dureeDiv = document.querySelector('#duree')
        dureeDiv.textContent = moment.duration(duree[2], 'seconds').hours() + 'h ' + moment.duration(duree[2], 'seconds').minutes() + 'm'
    }
}

const displayDebut = () => {
    if (window.location.href.split('=').pop() == 'navigation.html') {
        const goConst = document.querySelector('#depart')
        if (localStorage.getItem('lang') == 'fr') {
            goConst.textContent = moment(depart).format('DD MMMM YYYY, HH:mm')
        } else {
            goConst.textContent = moment(depart).format('MMMM Do YYYY, h:mm a')
        }
    }
}

const initStartButton = () => {
    const demarreur = document.querySelector('#start')
    started == 0 ? demarreur.textContent = '\u{1F511} Start' : demarreur.textContent = '\u{1F6D1} Stop'
}

const initTimes = () => {
    if (started == 1) {
        displayDebut()
        displayDuree()
    }
}
