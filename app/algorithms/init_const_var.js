//* DOM
const readoutUnits = {
    mph: 2.23694,
    kmh: 3.6,
    mphName: ' mph',
    kmhName: ' km/h',
    mi: 0.000621371,
    km: 0.001,
    miName: ' mi',
    kmName: ' km'
}

const appOpts = {
    readoutUnit: readoutUnits.kmh,
    readoutName: readoutUnits.km,
    watchId: null,
    wakeLock: null
}

const user = {
    id: localStorage.getItem('userId'),
    hash: localStorage.getItem('userHash'),
    default_lang: 'fr',
    default_driveStyle: 1
}

//* Starter.js
var interval_0 = true,
    interval_1 = true,
    interval_10 = true,
    interval_1000 = true,
    ticksTransition = true

//* transitions.js
const wrapper = '#pageWrapper',
    pagesFolder = 'pages/',
    pageVueParam = '?page=',
    subPagesFolder = 'subpages/',
    page1 = 'navigation.html',
    page2 = 'statistiques.html',
    page3 = 'pantheon.html',
    page4 = 'parametres.html'

//* noSleep_script.js
var noSleep = new NoSleep(),
    wakeLockEnabled = false

//* starter_functions.js
const gaugeTicks = 63
var pre_vitesse = 0,
    vitesse,
    pre_accuracy,
    newMaxSpeed = 0,
    pre_vMax = 0,
    tableau_vitesses = [],
    total_rows_tab = 1,
    pre_distance = 0,
    pre_vMoy = 0,
    vMax,
    vMoy,
    percentSpeed = 0,
    ticksOfLimit = 0,
    pre_limitation = 50,
    limitation,
    lat,
    long,
    oldValue = [0],
    pre_distance = 0,
    distance,
    duree = [],
    depart = "",
    started = 0

resetStarter = () => {
    pre_vitesse = 0
    newMaxSpeed = 0
    pre_vMax = 0
    tableau_vitesses = []
    total_rows_tab = 1
    pre_distance = 0
    pre_vMoy = 0
    percentSpeed = 0
    ticksOfLimit = 0
    oldValue = [0]
    pre_limitation = 50
    duree = [],
    depart = "",
    started = 1
}

//* statistiques.html
const loginSystemFolder = pagesFolder + subPagesFolder + 'login-system/',
    signUp_link = loginSystemFolder + 'inscription.html',
    signIn_link = loginSystemFolder + 'connexion.html'
