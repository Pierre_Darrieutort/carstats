const verifyConnexion = () => {
    if (localStorage.getItem('userId') == undefined || localStorage.getItem('userId') == null ||
        localStorage.getItem('userId') <= 0 || localStorage.getItem('userHash') == undefined ||
        localStorage.getItem('userHash') == null) {
        loadInId('#contenu', loginSystemFolder, 'preConnect.html', 0)
    } else {
        const userCheck = () => {
            $.ajax({
                url: 'server/ajax/connexion.php',
                type: 'POST',
                data: {
                    hidden_userId: localStorage.getItem('userId'),
                    hidden_userHash: localStorage.getItem('userHash')
                },
                success: function (data) {
                    var obj = JSON.parse(data)
                    if (obj.trustRes === 1) {} else {
                        localStorage.removeItem('userId')
                        localStorage.removeItem('userHash')
                        setTimeout(function () {
                            redirectLoadMethod(page2)
                        }, 1500)
                    }
                },
                error: function (data) {
                    // console.log(data)
                },
            })
        }
        userCheck()
        $('#statsWrapper').css('display', 'block')
    }
}
